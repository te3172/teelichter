---
title: 'Wie alles begann'
date: 2018-12-06T09:29:16+10:00
weight: 1
background: ''
align: right
---

Es war einmal vor langer langer Zeit... So beginnen Märchen, aber leider ist das Drama vor der Grenze von Europa kein Märchen. Auch geht es nicht um die Menschen die diese Aktion machen, sondern um die Opfer die noch immer wegen Unterlassener Hilfeleistung und fehlenden sicheren Fluchtrouten ums leben kommen. Die Menschen sterben weiter, sie ertrinken im Mittelmeer, begehen Suizid in Lagern und Gefängnissen oder werden von rassistischen Polizisten/Grenzbeamten/Neonazis getötet. Wir wollen diese hohe Anzahl von tragischen, aber vermeidbaren Opfern einer rassistischen Politik sichtbar machen.
