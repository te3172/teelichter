---
title: 'Fotos Januar 2021'
date: 2018-11-18T12:33:46+10:00
draft: false
weight: 1
heroHeading: 'Fotos Januar 2021'
heroSubHeading: 'von Anonym'
heroBackground: ''
thumbnail: 'work/schlachthof.jpg'
---

Hier können die Fotos betrachtet werden [was ist das?](#vexant-achivi) ist das nicht schön?

{{< vimeo 146022717 >}}

{{< tweet user="SanDiegoZoo" id="1453110110599868418" >}}

{{< youtube id="w7Ft2ymGmfc" title="A New Hugo Site in Under Two Minutes" >}}
{{< youtube id="w7Ft2ymGmfc" autoplay="true" >}}

Urheberrechte verbleiben bei den Fotografen.

{{< gallery match="images/*" sortOrder="desc" rowHeight="150" margins="5" thumbnailResizeOptions="600x600 q90 Lanczos" showExif=true previewType="blur" embedPreview="true" loadJQuery=True >}}